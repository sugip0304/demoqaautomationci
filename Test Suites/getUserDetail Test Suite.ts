<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>getUserDetail Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0e21a6ae-4440-401b-9edf-f5965f8f6f1c</testSuiteGuid>
   <testCaseLink>
      <guid>704e6be8-0aab-4ff5-8d39-ef6ffc05268f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/getUserDetail Test Case</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3edf1d2c-310d-4183-8950-dc1727daa06a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/getUserDetail Test Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>3edf1d2c-310d-4183-8950-dc1727daa06a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>userID</value>
         <variableId>5c3c3a27-309d-49f7-8ff5-94d5f3dcc810</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
